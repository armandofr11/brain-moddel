class AddFreshToZombies < ActiveRecord::Migration[5.0]
  def change
    add_column :zombies, :fresh, :boolean
  end
end
